local function insert_items(player)
    if settings.global["flexstart-10-burner-mining-drill"].value > 0 then
        player.insert{name = "burner-mining-drill", count = settings.global["flexstart-10-burner-mining-drill"].value}
    end
    if settings.global["flexstart-10-stone-furnace"].value > 0 then
        player.insert{name = "stone-furnace", count = settings.global["flexstart-10-stone-furnace"].value}
    end
    if settings.global["flexstart-15-wood"].value > 0 then
        player.insert{name = "wood", count = settings.global["flexstart-15-wood"].value}
    end
    if settings.global["flexstart-15-iron-plate"].value > 0 then
        player.insert{name = "iron-plate", count = settings.global["flexstart-15-iron-plate"].value}
    end
    if settings.global["flexstart-20-copper-plate"].value > 0 then
        player.insert{name = "copper-plate", count = settings.global["flexstart-20-copper-plate"].value}
    end
    if settings.global["flexstart-20-coal"].value > 0 then
        player.insert{name = "coal", count = settings.global["flexstart-20-coal"].value}
    end
    if settings.global["flexstart-20-stone"].value > 0 then
        player.insert{name = "stone", count = settings.global["flexstart-20-stone"].value}
    end
end


local function insert_equipment(player)
    if settings.global["flexstart-02-armortype"].value == "Modular Armor" then
        player.insert{name = "modular-armor", count = 1}
    end
    if settings.global["flexstart-02-armortype"].value == "Power Armor" then
        player.insert{name = "power-armor", count = 1}
    end
    if settings.global["flexstart-02-armortype"].value == "Power Armor Mk2" then
        player.insert{name = "power-armor-mk2", count = 1}
    end
    if settings.global["flexstart-03-weapontype"].value == "Pistol" then
        player.insert{name = "pistol", count = 1 }
    end
    if settings.global["flexstart-03-weapontype"].value == "Submachine Gun" then
        player.insert{name = "submachine-gun", count = 1 }
    end
    if settings.global["flexstart-04-ammotype"].value == "Regular" then
        if settings.global["flexstart-05-ammocount"].value > 0 then
            player.insert{name = "firearm-magazine", count = settings.global["flexstart-05-ammocount"].value}
        end
    end
    if settings.global["flexstart-04-ammotype"].value == "Piercing" then
        if settings.global["flexstart-05-ammocount"].value > 0 then
            player.insert{name = "piercing-rounds-magazine", count = settings.global["flexstart-05-ammocount"].value}
        end
    end

    -- Equipment:
    if settings.global["flexstart-construction-robot"].value > 0 then
        player.insert{name = "construction-robot", count = settings.global["flexstart-construction-robot"].value}
    end
    if settings.global["flexstart-solar-panel-equipment"].value > 0 then
        player.insert{name = "solar-panel-equipment", count = settings.global["flexstart-solar-panel-equipment"].value}
--         player.get_inventory(defines.inventory.player_armor).insert{name = "solar-panel-equipment", count = settings.global["flexstart-solar-panel-equipment"].value}
    end
    if settings.global["flexstart-fusion-reactor-equipment"].value > 0 then
        player.insert{name = "fusion-reactor-equipment", count = settings.global["flexstart-fusion-reactor-equipment"].value}
    end
    if settings.global["flexstart-energy-shield-equipment"].value > 0 then
        player.insert{name = "energy-shield-equipment", count = settings.global["flexstart-energy-shield-equipment"].value}
    end
    if settings.global["flexstart-energy-shield-mk2-equipment"].value > 0 then
        player.insert{name = "energy-shield-mk2-equipment", count = settings.global["flexstart-energy-shield-mk2-equipment"].value}
    end
    if settings.global["flexstart-battery-equipment"].value > 0 then
        player.insert{name = "battery-equipment", count = settings.global["flexstart-battery-equipment"].value}
    end
    if settings.global["flexstart-battery-mk2-equipment"].value > 0 then
        player.insert{name = "battery-mk2-equipment", count = settings.global["flexstart-battery-mk2-equipment"].value}
    end
    if settings.global["flexstart-personal-laser-defense-equipment"].value > 0 then
        player.insert{name = "personal-laser-defense-equipment", count = settings.global["flexstart-personal-laser-defense-equipment"].value}
    end
    if settings.global["flexstart-belt-immunity-equipment"].value then
        player.insert{name = "belt-immunity-equipment", count = 1}
    end
    if settings.global["flexstart-exoskeleton-equipment"].value > 0 then
        player.insert{name = "exoskeleton-equipment", count = settings.global["flexstart-exoskeleton-equipment"].value}
    end
    if settings.global["flexstart-personal-roboport-equipment"].value > 0 then
        player.insert{name = "personal-roboport-equipment", count = settings.global["flexstart-personal-roboport-equipment"].value}
    end
    if settings.global["flexstart-personal-roboport-mk2-equipment"].value > 0 then
        player.insert{name = "personal-roboport-mk2-equipment", count = settings.global["flexstart-personal-roboport-mk2-equipment"].value}
    end

    if settings.global["flexstart-night-vision-equipment"].value then
--         player.get_inventory(defines.inventory.player_armor).insert{name = "solar-panel-equipment", count = 1}
--         player.get_inventory(defines.inventory.player_armor).insert{name = "solar-panel-equipment", count = 1}
        player.insert{name = "night-vision-equipment", count = 1}
    end

--     if settings.global["flexstart-"].value > 0 then
--         player.insert{name = "", count = settings.global["flexstart-"].value}
--     end

end


script.on_event(defines.events.on_player_created, function(event)
    local player = game.players[event.player_index]
    player.character.clear_items_inside()
    insert_equipment(player)
    insert_items(player)
end)


script.on_event(defines.events.on_player_respawned, function(event)
    if settings.global["flexstart-01-onrespawn"].value then
        local player = game.players[event.player_index]
        player.character.clear_items_inside()
        insert_equipment(player)
    end
end)

    

--     if settings.global["flexstart-"].value > 0 then
--         player.insert{name = "", count = settings.global["flexstart-"].value}
--     end

-- burner miner 1
-- stone furnace 1
-- wood 1
-- iron 8
