data:extend{
    {
        type = "bool-setting",
        name = "flexstart-01-onrespawn",
        setting_type = "runtime-global",
        default_value = true
    },
    {
        type = "string-setting",
        name = "flexstart-02-armortype",
        setting_type = "runtime-global",
        default_value = "None",
        allowed_values = { "None", "Modular Armor", "Power Armor", "Power Armor Mk2" }
    }, 
    {
        type = "string-setting",
        name = "flexstart-03-weapontype",
        setting_type = "runtime-global",
        default_value = "Pistol",
        allowed_values = { "None", "Pistol", "Submachine Gun" }
    }, 
    {
        type = "string-setting",
        name = "flexstart-04-ammotype",
        setting_type = "runtime-global",
        default_value = "Regular",
        allowed_values = { "Regular", "Piercing" }
    }, 
    {
        type = "int-setting",
        name = "flexstart-05-ammocount",
        setting_type = "runtime-global",
        default_value = 100,
        maximum_value = 10000,
    },
    
    -- Default Starting Items:
    {
        type = "int-setting",
        name = "flexstart-10-burner-mining-drill",
        setting_type = "runtime-global",
        default_value = 4,
        maximum_value = 1000,
    },   
    {
        type = "int-setting",
        name = "flexstart-10-stone-furnace",
        setting_type = "runtime-global",
        default_value = 4,
        maximum_value = 1000,
    },
    {
        type = "int-setting",
        name = "flexstart-15-wood",
        setting_type = "runtime-global",
        default_value = 100,
        maximum_value = 5000,
    },
    {
        type = "int-setting",
        name = "flexstart-15-iron-plate",
        setting_type = "runtime-global",
        default_value = 50,
        maximum_value = 5000,
    },

    -- Raw Materials:
    {
        type = "int-setting",
        name = "flexstart-20-copper-plate",
        setting_type = "runtime-global",
        default_value = 0,
        maximum_value = 5000,
    },
    {
        type = "int-setting",
        name = "flexstart-20-coal",
        setting_type = "runtime-global",
        default_value = 100,
        maximum_value = 5000,
    },
    {
        type = "int-setting",
        name = "flexstart-20-stone",
        setting_type = "runtime-global",
        default_value = 0,
        maximum_value = 5000,
    },

-- Equipment:
    {
        type = "int-setting",
        name = "flexstart-construction-robot",
        setting_type = "runtime-global",
        default_value = 0,
        maximum_value = 1000,
    },
    {
        type = "int-setting",
        name = "flexstart-solar-panel-equipment",
        setting_type = "runtime-global",
        default_value = 0,
        maximum_value = 100,
    },
    {
        type = "int-setting",
        name = "flexstart-fusion-reactor-equipment",
        setting_type = "runtime-global",
        default_value = 0,
        maximum_value = 2,
    },
    {
        type = "int-setting",
        name = "flexstart-energy-shield-equipment",
        setting_type = "runtime-global",
        default_value = 0,
        maximum_value = 10,
    },
    {
        type = "int-setting",
        name = "flexstart-energy-shield-mk2-equipment",
        setting_type = "runtime-global",
        default_value = 0,
        maximum_value = 10,
    },
    {
        type = "int-setting",
        name = "flexstart-battery-equipment",
        setting_type = "runtime-global",
        default_value = 0,
        maximum_value = 10,
    },
    {
        type = "int-setting",
        name = "flexstart-battery-mk2-equipment",
        setting_type = "runtime-global",
        default_value = 0,
        maximum_value = 50,
    },
    {
        type = "int-setting",
        name = "flexstart-personal-laser-defense-equipment",
        setting_type = "runtime-global",
        default_value = 0,
        maximum_value = 10,
    },
    {
        type = "bool-setting",
        name = "flexstart-belt-immunity-equipment",
        setting_type = "runtime-global",
        default_value = false,
    },
    {
        type = "int-setting",
        name = "flexstart-exoskeleton-equipment",
        setting_type = "runtime-global",
        default_value = 0,
        maximum_value = 10,
    },

    {
        type = "int-setting",
        name = "flexstart-personal-roboport-equipment",
        setting_type = "runtime-global",
        default_value = 0,
        maximum_value = 10,
    },
    {
        type = "int-setting",
        name = "flexstart-personal-roboport-mk2-equipment",
        setting_type = "runtime-global",
        default_value = 0,
        maximum_value = 10,
    },
    {
        type = "bool-setting",
        name = "flexstart-night-vision-equipment",
        setting_type = "runtime-global",
        default_value = false,
    },

--     {
--         type = "int-setting",
--         name = "flexstart-",
--         setting_type = "runtime-global",
--         default_value = 0,
--         maximum_value = 100,
--     },

}
