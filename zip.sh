FOLDER=$1
zip -ru `cat $FOLDER/info.json | jq -rc '"\(.name)_\(.version).zip"'` $FOLDER
